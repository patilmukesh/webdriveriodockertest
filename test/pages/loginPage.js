const userDetails=require('../../testdata/userDetails');

export class LoginPage {
  get userNameTextBox() {return browser.$('//input[@type="text"]');}
  get passwordTextBox() {return browser.$('//input[@type="password"]');}
  get submitButton() {return browser.$('//input[@type="submit"]');}
  loginAs(userType){
    this.userNameTextBox.setValue(userDetails[userType].userId);
    this.passwordTextBox.setValue(userDetails[userType].password)
    this.submitButton.click();
  }
}
const login = new LoginPage();
export { login };
