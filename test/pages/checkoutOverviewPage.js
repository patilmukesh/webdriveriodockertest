import {ShoppingPage} from "./shoppingPage";
import * as assert from "assert";

const _ = require('lodash');
const util = require('../utilities/utilities');

export class CheckoutOverviewPage extends ShoppingPage{
  get checkouItems() { return browser.$$('//div[@class="cart_item"]');}
  get itemTotalPreTax() {return browser.$('//div[@class="summary_subtotal_label"]');}
  verifyOverviewPageDisp() {
    expect(util.getHeader()).toHaveText('Checkout: Overview');
    expect(util.getButtonElement('CANCEL')).toBeDisplayed();
    expect(util.getButtonElement('FINISH')).toBeDisplayed();
    expect(this.checkouItems).toBeElementsArrayOfSize(super.prodAddedCartArray.length);
  }
  verifyItemsCheckout() {
    let cartPageItems = this.checkouItems;
    assert.equal(cartPageItems.length,super.prodAddedCartArray.length,'Cart Page element are matching with added on product page');
    for (let i = 0; i < super.prodAddedCartArray.length; i++) {
      let cardPageItemName = util.getChildElementTextValue(cartPageItems[i],super.itemNameXPath);
      let cardPageItemPrice = util.getChildElementTextValue(cartPageItems[i],super.itemPriceXPath);
      assert.notEqual(_.findIndex(super.prodAddedCartArray, (prodElem) => (prodElem[0] === cardPageItemName && prodElem[1] === cardPageItemPrice)),-1,'Element is found in array');
    }
    console.log('UI sum value',this.itemTotalPreTax.getText().toString().split('Item total: $')[1]);
    console.log('array sum',_.sumBy(super.prodAddedCartArray, (price) => (parseFloat(price[1].replace(/[^0-9\.]+/g, '')))));
    assert.equal(this.itemTotalPreTax.getText().toString().split('Item total: $')[1],_.sumBy(super.prodAddedCartArray, (price) => parseFloat(price[1].replace(/[^0-9\.]+/g,""))),'Sum before tax is matching with added products');
  }
}
const checkoutOverviewPage = new CheckoutOverviewPage();
export {checkoutOverviewPage};
