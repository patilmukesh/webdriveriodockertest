import * as assert from "assert";

const util = require('../utilities/utilities');

export class YourInformationPage{
  get firstNameElement() {return browser.$("#first-name");}
  get lastNameElement() {return browser.$("#last-name");}
  get zipElement() {return browser.$("#postal-code");}
  get continueButton() { return browser.$('//input[@class="btn_primary cart_button"]');}
  fillInfoAndContinue(fName, lName, zip) {
    this.firstNameElement.setValue(fName);
    this.lastNameElement.setValue(lName);
    this.zipElement.setValue(zip);
    this.continueButton.click();
  }

  verifyInfoPageDisp() {
    expect(util.getHeader()).toHaveText('Checkout: Your Information');
    expect(this.firstNameElement).toBeDisplayed();
    expect(this.lastNameElement).toBeDisplayed();
    expect(this.firstNameElement).toBeDisplayed();
    expect(this.zipElement).toBeDisplayed();
  }
}
const yourInformationPage = new YourInformationPage();
export {yourInformationPage};
