import * as assert from "assert";
const _ = require('lodash');
import {ShoppingPage} from "./shoppingPage";

const util = require('../utilities/utilities');
let prodAddedCart=[];

export class ShoppingCartPage extends ShoppingPage{
  constructor() {
    super();
    prodAddedCart=super.prodAddedCartArray;
  }
  get cartPageItemList() {return browser.$$('//div[@class="cart_item"]');}
  get removeCartButtonXPath() {return 'button[class="btn_secondary cart_button"]';}
  verifyShoppingCartPage() {
      /**Verify cart page display**/
      expect(util.getHeader()).toHaveText('Your Cart');
      browser.waitUntil(()=>(util.getButtonElement('Continue Shopping').isDisplayed()));
      expect(util.getButtonElement('Continue Shopping')).toBeDisplayed();
  }
  verifyCartItems() {

    /**Verify items displayed on cart page with selected on product page**/
    let cartPageItems = this.cartPageItemList;
    assert.equal(cartPageItems.length,super.prodAddedCartArray.length,'Cart Page element are matching with added on product page');
    for (let i = 0; i < super.prodAddedCartArray.length; i++) {
      let cardPageItemName = util.getChildElementTextValue(cartPageItems[i],super.itemNameXPath);
      let cardPageItemPrice = '$'+util.getChildElementTextValue(cartPageItems[i],super.itemPriceXPath);
      console.log(_.findIndex(super.prodAddedCartArray, (prodElem) => (prodElem[0] ===cardPageItemName && prodElem[1] === cardPageItemPrice)));
      assert.notEqual(_.findIndex(super.prodAddedCartArray, (prodElem) => (prodElem[0] === cardPageItemName && prodElem[1] === cardPageItemPrice)),-1,'Element is found in array');
    }
  }
  removeSelectedItem(itemNumber) {
    let itemToDelName='';
    let itemToDelPrice='';
    if(!(itemNumber>super.prodAddedCartArray.length||itemNumber===0)) {
      itemToDelName=util.getChildElementTextValue(this.cartPageItemList[itemNumber-1],super.itemNameXPath);
      itemToDelPrice='$'+util.getChildElementTextValue(this.cartPageItemList[itemNumber-1],super.itemPriceXPath);
      util.getChildElement(this.cartPageItemList[itemNumber-1],this.removeCartButtonXPath).click();
    }
    else{
      assert.equal(itemNumber,-1,'Incorrect item number');
    }
    browser.waitUntil(()=>(util.getButtonElement('Continue Shopping').isDisplayed()));
    expect(util.getHeader()).toHaveText('Your Cart');
    super.delElemFromArray(itemToDelName,itemToDelPrice);
    expect(this.cartPageItemList).toBeElementsArrayOfSize(super.prodAddedCartArray.length);
    util.getButtonElement('Continue Shopping').click();
  }
  navToCheckoutInfo() {
    util.getButtonElement('CHECKOUT').click();
  }
}
const shoppingCartPage=new ShoppingCartPage();
export {shoppingCartPage};
