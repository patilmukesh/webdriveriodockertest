Feature: This feature is to validate shopping website smoke or quick testing.
  @Smoke
  Scenario Outline: To validate shopping site opens and user can naviagate till Cart page.
    Given I open Swaglab login page
    When "<UserType>" login into website using user id and password assigned
    Then user verify 'Products' landing page is displayed
    Then user selects "<SortOption>" to high sort option
    And user navigate to cart page
    And user verify 'Shopping Cart' page is displayed
    Examples:
      |UserType  |SortOption|
      |standarduser|lowtohigh|
