const lookupValues = require('./lookupValues');
const utilities = {
  getHeader() {
    return browser.$('//div[@class="subheader"]');
  },
  getChildElementTextValue(element,xpath) {
    return element.$(xpath).getText();
  },
  getChildElement(element,xpath) {
    return element.$(xpath);
  },
  verifySortedProd(sortOption,prodItemList) {
    let ascending = null;
    const determineOrder = prodItemList => {
      let nextArr = prodItemList.slice(1);
      console.log(nextArr.length);
      for(var i = 0; i < nextArr.length; i++) {
        if(parseFloat(this.getChildElementTextValue(nextArr[i],'div[class="inventory_item_price"]').replace(/[^0-9\.]+/g,"")) === parseFloat(this.getChildElementTextValue(prodItemList[i],'div[class="inventory_item_price"]').replace(/[^0-9\.]+/g,""))){
          continue;
        }else if(ascending === null) {
          ascending = parseFloat(this.getChildElementTextValue(nextArr[i],'div[class="inventory_item_price"]').replace(/[^0-9\.]+/g,"")) > parseFloat(this.getChildElementTextValue(prodItemList[i],'div[class="inventory_item_price"]').replace(/[^0-9\.]+/g,""));
        }else if (ascending !== parseFloat(this.getChildElementTextValue(nextArr[i],'div[class="inventory_item_price"]').replace(/[^0-9\.]+/g,"")) > parseFloat(this.getChildElementTextValue(prodItemList[i],'div[class="inventory_item_price"]').replace(/[^0-9\.]+/g,""))){
          return 'unsorted';
        };
      }
      if(ascending === null){
        return 'all items are equal';
      };
      return ascending ? 'ascending' : 'descending';
    };
    return determineOrder(prodItemList);
  },
  getButtonElement(buttonName) {
    return browser.$('//*[contains(text(),'+"'"+buttonName+"'"+')]');
  }
}
module.exports = utilities;
