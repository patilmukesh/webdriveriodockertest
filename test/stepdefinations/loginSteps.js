var {Given} = require('cucumber');

import { login } from '../pages/loginPage.js';

Given(/^"([^"]*)" login into website using user id and password assigned$/,  (userType)=> {
  login.loginAs(userType);
});
Given(/^I open Swaglab login page$/, function () {
  browser.url(browser.options.baseUrl);
  browser.maximizeWindow();
});
