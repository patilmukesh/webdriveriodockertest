var {When,Then} = require('cucumber');
import {shoppingPage} from "../pages/shoppingPage";
import {shoppingCartPage} from "../pages/shoppingCartPage";
import {yourInformationPage} from "../pages/yourInformationPage";
import {checkoutOverviewPage} from "../pages/checkoutOverviewPage";

Then(/^user verify 'Products' landing page is displayed$/, function () {
  shoppingPage.verifyProductPage();
});
When(/^user selects "([^"]*)" to high sort option$/, function (sortOption) {
  shoppingPage.selectSortOption(sortOption);
});
Then(/^user verify all products displayed in "([^"]*)" order$/,  (sortOption)=> {
  shoppingPage.verifySortedProduct(sortOption);
});
Then(/^user adds "([^"]*)" to Cart by selecting 'Add to Cart' option$/,  (numberOfItems) =>{
  shoppingPage.addProductsToCart(numberOfItems);
});
Then(/^user verify all selected items are added in shopping cart page$/, function () {
  shoppingPage.navToCartPage();
  shoppingCartPage.verifyShoppingCartPage();
  shoppingCartPage.verifyCartItems();
});
Then(/^user remove "([^"]*)" item from shopping cart and selects continue shopping$/,  (itemNumber) =>{
  shoppingCartPage.removeSelectedItem(itemNumber);
});
Then(/^user adds an item to Cart by selecting 'Add to Cart' option$/, function () {
  shoppingPage.addProductsToCart(1);
});
Then(/^user finishes checkout by submitting "([^"]*)", "([^"]*)" and "([^"]*)" on Information page$/,  (fName,lName,zip)=> {
  shoppingPage.navToCartPage();
  shoppingCartPage.navToCheckoutInfo();
  yourInformationPage.fillInfoAndContinue(fName,lName,zip);
});
Then(/^user navigate to Information page to submit by filling "([^"]*)", "([^"]*)" and "([^"]*)"$/, (fName,lName,zip)=> {
  shoppingPage.navToCartPage();
  shoppingCartPage.navToCheckoutInfo();
  yourInformationPage.verifyInfoPageDisp();
  yourInformationPage.fillInfoAndContinue(fName,lName,zip);
});
Then(/^user verify item total price on Overview Page$/,  ()=> {
  checkoutOverviewPage.verifyOverviewPageDisp();
  checkoutOverviewPage.verifyItemsCheckout();
});
Then(/^user navigate to cart page$/,  ()=> {
  shoppingCartPage.navToCartPage();
});
Then(/^user verify 'Shopping Cart' page is displayed$/,  () =>{
  shoppingCartPage.verifyShoppingCartPage();
});
